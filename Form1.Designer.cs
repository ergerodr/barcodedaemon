﻿namespace TestPrint
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナ変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナで生成されたコード

        /// <summary>
        /// デザイナ サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディタで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnPrint = new System.Windows.Forms.Button();
            this.printerLabel = new System.Windows.Forms.Label();
            this.cmbPrinter = new System.Windows.Forms.ComboBox();
            this.barcodeLabel = new System.Windows.Forms.Label();
            this.cmbBarcode = new System.Windows.Forms.ComboBox();
            this.quantityLabel = new System.Windows.Forms.Label();
            this.QntyTextBox = new System.Windows.Forms.TextBox();
            this.lotTextBox = new System.Windows.Forms.TextBox();
            this.lotLabel = new System.Windows.Forms.Label();
            this.itemTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.scanInput = new System.Windows.Forms.TextBox();
            this.scanButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.createBarcode = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.uomLabel = new System.Windows.Forms.Label();
            this.uomCombo = new System.Windows.Forms.ComboBox();
            this.plLabel = new System.Windows.Forms.Label();
            this.plNumber = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(16, 348);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(117, 25);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Print label";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printerLabel
            // 
            this.printerLabel.AutoSize = true;
            this.printerLabel.Location = new System.Drawing.Point(9, 17);
            this.printerLabel.Name = "printerLabel";
            this.printerLabel.Size = new System.Drawing.Size(37, 13);
            this.printerLabel.TabIndex = 4;
            this.printerLabel.Text = "Printer";
            // 
            // cmbPrinter
            // 
            this.cmbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter.FormattingEnabled = true;
            this.cmbPrinter.Items.AddRange(new object[] {
            "Simple",
            "Decoration Frame",
            "QR"});
            this.cmbPrinter.Location = new System.Drawing.Point(52, 14);
            this.cmbPrinter.Name = "cmbPrinter";
            this.cmbPrinter.Size = new System.Drawing.Size(252, 21);
            this.cmbPrinter.TabIndex = 5;
            this.cmbPrinter.SelectedIndexChanged += new System.EventHandler(this.cmbPrinter_SelectedIndexChanged);
            // 
            // barcodeLabel
            // 
            this.barcodeLabel.AutoSize = true;
            this.barcodeLabel.Location = new System.Drawing.Point(338, 14);
            this.barcodeLabel.Name = "barcodeLabel";
            this.barcodeLabel.Size = new System.Drawing.Size(47, 13);
            this.barcodeLabel.TabIndex = 7;
            this.barcodeLabel.Text = "Barcode";
            // 
            // cmbBarcode
            // 
            this.cmbBarcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBarcode.FormattingEnabled = true;
            this.cmbBarcode.Items.AddRange(new object[] {
            "FG",
            "WIP",
            "RackTag",
            "Raw Material"});
            this.cmbBarcode.Location = new System.Drawing.Point(391, 12);
            this.cmbBarcode.Name = "cmbBarcode";
            this.cmbBarcode.Size = new System.Drawing.Size(174, 21);
            this.cmbBarcode.TabIndex = 10;
            // 
            // quantityLabel
            // 
            this.quantityLabel.AutoSize = true;
            this.quantityLabel.Location = new System.Drawing.Point(13, 103);
            this.quantityLabel.Name = "quantityLabel";
            this.quantityLabel.Size = new System.Drawing.Size(46, 13);
            this.quantityLabel.TabIndex = 11;
            this.quantityLabel.Text = "Quantity";
            // 
            // QntyTextBox
            // 
            this.QntyTextBox.Location = new System.Drawing.Point(92, 100);
            this.QntyTextBox.Name = "QntyTextBox";
            this.QntyTextBox.Size = new System.Drawing.Size(60, 20);
            this.QntyTextBox.TabIndex = 12;
            // 
            // lotTextBox
            // 
            this.lotTextBox.Location = new System.Drawing.Point(92, 140);
            this.lotTextBox.Name = "lotTextBox";
            this.lotTextBox.Size = new System.Drawing.Size(100, 20);
            this.lotTextBox.TabIndex = 14;
            // 
            // lotLabel
            // 
            this.lotLabel.AutoSize = true;
            this.lotLabel.Location = new System.Drawing.Point(13, 143);
            this.lotLabel.Name = "lotLabel";
            this.lotLabel.Size = new System.Drawing.Size(62, 13);
            this.lotLabel.TabIndex = 13;
            this.lotLabel.Text = "Lot Number";
            // 
            // itemTextBox
            // 
            this.itemTextBox.Location = new System.Drawing.Point(92, 60);
            this.itemTextBox.Name = "itemTextBox";
            this.itemTextBox.Size = new System.Drawing.Size(100, 20);
            this.itemTextBox.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Item Number";
            // 
            // scanInput
            // 
            this.scanInput.Location = new System.Drawing.Point(366, 348);
            this.scanInput.Name = "scanInput";
            this.scanInput.Size = new System.Drawing.Size(214, 20);
            this.scanInput.TabIndex = 18;
            // 
            // scanButton
            // 
            this.scanButton.Location = new System.Drawing.Point(273, 346);
            this.scanButton.Name = "scanButton";
            this.scanButton.Size = new System.Drawing.Size(75, 23);
            this.scanButton.TabIndex = 19;
            this.scanButton.Text = "Scan";
            this.scanButton.UseVisualStyleBackColor = true;
            this.scanButton.Click += new System.EventHandler(this.scanButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(315, 103);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 163);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // createBarcode
            // 
            this.createBarcode.Location = new System.Drawing.Point(26, 218);
            this.createBarcode.Name = "createBarcode";
            this.createBarcode.Size = new System.Drawing.Size(92, 26);
            this.createBarcode.TabIndex = 21;
            this.createBarcode.Text = "Create Barcode";
            this.createBarcode.UseVisualStyleBackColor = true;
            this.createBarcode.Click += new System.EventHandler(this.createBarcode_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(163, 218);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(92, 26);
            this.clearButton.TabIndex = 22;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // uomLabel
            // 
            this.uomLabel.AutoSize = true;
            this.uomLabel.Location = new System.Drawing.Point(170, 103);
            this.uomLabel.Name = "uomLabel";
            this.uomLabel.Size = new System.Drawing.Size(32, 13);
            this.uomLabel.TabIndex = 23;
            this.uomLabel.Text = "UOM";
            // 
            // uomCombo
            // 
            this.uomCombo.FormattingEnabled = true;
            this.uomCombo.Items.AddRange(new object[] {
            "CS",
            "LB",
            "EA",
            "OZ",
            "IN"});
            this.uomCombo.Location = new System.Drawing.Point(208, 99);
            this.uomCombo.Name = "uomCombo";
            this.uomCombo.Size = new System.Drawing.Size(47, 21);
            this.uomCombo.TabIndex = 24;
            // 
            // plLabel
            // 
            this.plLabel.AutoSize = true;
            this.plLabel.Location = new System.Drawing.Point(13, 184);
            this.plLabel.Name = "plLabel";
            this.plLabel.Size = new System.Drawing.Size(73, 13);
            this.plLabel.TabIndex = 25;
            this.plLabel.Text = "Pallet Number";
            // 
            // plNumber
            // 
            this.plNumber.Location = new System.Drawing.Point(92, 181);
            this.plNumber.Name = "plNumber";
            this.plNumber.Size = new System.Drawing.Size(41, 20);
            this.plNumber.TabIndex = 26;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 385);
            this.Controls.Add(this.plNumber);
            this.Controls.Add(this.plLabel);
            this.Controls.Add(this.uomCombo);
            this.Controls.Add(this.uomLabel);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.createBarcode);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.scanButton);
            this.Controls.Add(this.scanInput);
            this.Controls.Add(this.itemTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lotTextBox);
            this.Controls.Add(this.lotLabel);
            this.Controls.Add(this.QntyTextBox);
            this.Controls.Add(this.quantityLabel);
            this.Controls.Add(this.cmbBarcode);
            this.Controls.Add(this.barcodeLabel);
            this.Controls.Add(this.cmbPrinter);
            this.Controls.Add(this.printerLabel);
            this.Controls.Add(this.btnPrint);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "SCN QR Print and Scan";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.Label printerLabel;
		private System.Windows.Forms.ComboBox cmbPrinter;
		private System.Windows.Forms.Label barcodeLabel;
        private System.Windows.Forms.ComboBox cmbBarcode;
        private System.Windows.Forms.Label quantityLabel;
        private System.Windows.Forms.TextBox QntyTextBox;
        private System.Windows.Forms.TextBox lotTextBox;
        private System.Windows.Forms.Label lotLabel;
        private System.Windows.Forms.TextBox itemTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox scanInput;
        private System.Windows.Forms.Button scanButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button createBarcode;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label uomLabel;
        private System.Windows.Forms.ComboBox uomCombo;
        private System.Windows.Forms.Label plLabel;
        private System.Windows.Forms.TextBox plNumber;
    }
}

