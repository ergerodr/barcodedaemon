using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Net;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using bpac;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using System.Printing;

namespace TestPrint
{
    public partial class Form1 : Form
    {
        private const int FG_CODE = 0;
        private const int WIP_CODE = 1;
        private const int RM_CODE = 3;

        private static string TEMPLATE_DIRECTORY = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData); // Template file path
        private string QR_FILE_LOC = TEMPLATE_DIRECTORY + @"\temp_img.png";
        private string TEMPLATE_QR = TEMPLATE_DIRECTORY + @"\QR-code-template.LBX";
        private const string WARNING = "No Supported Printers Found";
        private const string BROTHER_WARNING = "No Supported Brother Label Printers Found. Please install one, restart the application and try again";
        private const string TEST_BARCODE_PATH = @"C:\Users\erodriguez1\Desktop\BARCODE_STRING_DATA.csv";

        private const string RM_PATTERN = @"[0-9\-]{1,} [0-9a-zA-Z]{1,} [0-9]{1,} (EA|LB|OZ|IN)";
        private const string WIP_PATTERN = @"[0-9]{1,} [A-Z0-9a-z]{1,} [0-9]{1,} (EA|LB) PL[0-9]{1,}";
        private const string FG_PATTERN = @"[A-Z0-9]{1,} [A-Z0-9]{1,} [0-9]{1,} (CS|PL) [PL0-9]{1,}";
        private readonly static Uri QR_CODE_API_URL = new Uri("http://qrickit.com/api/qr.php?d=");
        Image qr_img;
        bpac.Document doc;

        string[] printerNames;

        public Form1()
        {
            doc = new Document();
            doc.Open(TEMPLATE_QR);
            doc.Printer.GetInstalledPrinters();
            object[] printers = (object[])doc.Printer.GetInstalledPrinters();

            if (printers != null)
            {
                int i = 0;
                printerNames = new String[printers.Length];

                foreach (object printer in printers)
                {
                    printerNames[i] = printer.ToString();
                    i++;
                }
            }
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.cmbPrinter.SelectedIndex = 0;
            this.cmbPrinter.Items.Clear();
            if (printerNames != null)
                this.cmbPrinter.Items.AddRange(printerNames);
            else
            {
                this.cmbPrinter.Items.AddRange(new string[] { WARNING });
                MessageBox.Show(BROTHER_WARNING);
                this.btnPrint.Enabled = false; 
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
      
            bpac.IObject obj = doc.Objects[0];

            obj.SetData(0, QR_FILE_LOC, 0);

                
            doc.StartPrint("", PrintOptionConstants.bpoDefault);
            doc.PrintOut(1, PrintOptionConstants.bpoDefault);
            doc.EndPrint();
      
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void scanButton_Click(object sender, EventArgs e)
        {
            String scanOutput = scanInput.Text.Trim(new[] { '|', '~' });
            scanInput.Text = scanOutput;
            try
            {
                verifyBarcode(scanOutput);
            } catch(ArithmeticException except)
            {
                MessageBox.Show("Invalid Barcode: " + except.Message);
            }
        }

        public void verifyBarcode(string scanOutput)
        {
            Regex rm_regex = new Regex(RM_PATTERN);
            Regex wip_regex = new Regex(WIP_PATTERN);
            Regex fg_regex = new Regex(FG_PATTERN);

            if (fg_regex.IsMatch(scanOutput))
            {
                cmbBarcode.SelectedItem = "FG";
                string[] chunks = scanOutput.Split(null);

                populateBarcode(chunks, scanOutput);
            }
            else if (wip_regex.IsMatch(scanOutput))
            {
                cmbBarcode.SelectedItem = "WIP";
                string[] chunks = scanOutput.Split(null);

                populateBarcode(chunks, scanOutput);
            }
            else if (rm_regex.IsMatch(scanOutput))
            {
                cmbBarcode.SelectedItem = "Raw Material";
                string[] chunks = scanOutput.Split(null);

                populateBarcode(chunks, scanOutput);
            }
            else
            {
                throw new ArithmeticException(message:scanOutput);
            }
        }

        private void createBarcode_Click(object sender, EventArgs e)
        {
            String Barcode = itemTextBox.Text.Trim() + " " +
                             lotTextBox.Text.Trim() + " " +
                             QntyTextBox.Text.Trim() + " " +
                             uomCombo.Text.Trim() + " " +
                             plNumber.Text.Trim();

            scanInput.Text = Barcode;
            scanButton_Click(sender, e);
        }

        private String returnUOM(string scanText)
        {
            if (scanText.Contains("CS"))
                return "CS";
            else if (scanText.Contains("EA"))
                return "EA";
            else if (scanText.Contains("OZ"))
                return "OZ";
            else if (scanText.Contains("IN"))
                return "IN";
            else
                return "LB";
        }
        
        private void populateBarcode(string[] chunks, string scanOutput)
        {

            itemTextBox.Text = chunks[0];
            lotTextBox.Text = chunks[1];
            QntyTextBox.Text = chunks[2];
            uomCombo.SelectedItem = returnUOM(scanOutput);
            plNumber.Text = getPlNumber(scanOutput);

            scanInput.Text = "";

            string queryString = scanOutput;
            pictureBox1.Image = Get_qr_code_image(queryString);
            
        }

        private String getPlNumber(string scanText)
        {
            Regex plregex = new Regex("PL[0-9]{1,2}");
            Match results = plregex.Match(scanText);

            if (results.Success)
            {
                Console.WriteLine(results.Groups[0].Value);
                Console.WriteLine(results.Groups[1].Value);
            }
            return results.Groups[0].Value;
        }

        private Image Get_qr_code_image(String queryString)
        {
            HttpWebRequest qrCodeRequest =
                (HttpWebRequest)WebRequest.Create(QR_CODE_API_URL + queryString);

            WebResponse response = qrCodeRequest.GetResponse();
            Image qr_copy;
            // Get image stream associated with the response.
            using (Stream receiveStream = response.GetResponseStream())
            {
                // Create image from stream
                using (qr_img = Image.FromStream(receiveStream))
                {
                    qr_copy = new Bitmap(qr_img);
                    qr_copy.Save(QR_FILE_LOC);
                }
            }
            //response.Close();
            //receiveStream.Close();
            return qr_copy;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            itemTextBox.Text = "";
            lotTextBox.Text = "";
            QntyTextBox.Text = "";
            uomCombo.SelectedItem = null;
            cmbBarcode.SelectedItem = null; 
            plNumber.Text = "";
            pictureBox1.Image = null;
        }

        private void cmbPrinter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbPrinter.Text != WARNING) { 
                bool result = doc.SetPrinter(cmbPrinter.Text, false);
            }
        }
    }

    public class testSetup
    {
        public List<string> getTestBarcodes(string path)
        {
            TextFieldParser parser = new TextFieldParser(@path);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            List<string[]> parsedData = new List<string[]>();
            string[] fields;

            string line = parser.ReadLine();

            try
            {
                while (!parser.EndOfData)
                {
                    fields = parser.ReadFields();
                    parsedData.Add(fields);    
                }
                parser.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            List<string> barcodeList = new List<string>();
            foreach (string[] element in parsedData)
            {
                barcodeList.Add(element[7]);
            }
            return barcodeList;
        }
    }
}
